var results = {};
var schedule_status = 'not running';

function clickStop(type) {
    $.ajax({
        async: false,
        url: '/' + type,
        type: 'POST',
        dataType: 'json',
        crossDomain: true,
        success: function (data) {
            //schedule_status = data;
            console.log(data);
        }
    });
}

$(document).ready(function () {

    var progress_bar = function (type) {
        $.ajax({
            async: false,
            url: '/progress',
            data: 'type=' + type,
            type: 'GET',
            dataType: 'json',
            crossDomain: true,
            success: function (data) {
                results = data;
                console.log(data);
            }
        });

        results.number_of_scraped_pages = (results.number_of_scraped_pages) ? results.number_of_scraped_pages : 0;
        results.number_of_start_urls = (results.number_of_start_urls) ? results.number_of_start_urls : 0;
        results.scraper_status = (results.scraper_status) ? results.scraper_status : "not running";
        results.percentage = (results.percentage) ? results.percentage : 0;
        results.number_of_scraped_items = (results.number_of_scraped_items) ? results.number_of_scraped_items : 0;
        results.file = (results.file) ? results.file: '...';

        $('#'+ type + ' .progress-bar').attr('aria-valuenow', results.number_of_scraped_pages)
            .attr('aria-valuemax', results.number_of_start_urls);
        if (results.scraper_status == "finished") {
            $('#'+ type + ' .progress-bar').css('width', '100%')
                .text("Done");
        }
        else if (results.percentage === "Please wait...") {
            $('#'+ type + ' .progress-bar').css('width', '100%')
                .text(results.percentage);
        }
        else {
            $('#'+ type + ' .progress-bar').css('width', results.percentage + '%')
                .text(results.percentage + '%');
        }
        $('#'+ type + ' .status_scraper').text("Status: " + results.scraper_status);
        $('#'+ type + ' .item_scraped').text("Sites completed: " + results.number_of_scraped_items + ",   ");
        $('#'+ type + ' .page_scraped').text("Pages processed: " + results.number_of_scraped_pages + ',   ');
        $('#'+ type + ' .number_of_inputs').text("Total sites to process: " + results.number_of_start_urls + ",   ");
        $('#'+ type + ' .filename').text("" + results.file);
    };
    progress_bar('example_spider');

    setInterval(function() {progress_bar('example_spider');}, 10000);
});