# -*- coding: utf-8 -*-
from flask import url_for
import json
from ..libs.tests import assert_status_with_message



class TestFilePool(object):
    def test_upload_page(self, client):
        """ Home page should respond with a success 200. """
        response = client.get('/upload/')
        assert response.status_code == 200

    def test_download_page(self, client):
        """ Terms page should respond with a success 200. """
        response = client.get('/download/')
        assert response.status_code == 200


class TestProgress(object):
    def test_progress(self, client):
        """ Home page should respond with a success 200. """
        response = client.get('/progress?type=example_spider')
        assert response.status_code == 200
        assert response.json == {}


class TestStopCrawlingSpider(object):
    def test_stop_crawling_spider(self, client):
        """ Home page should respond with a success 200. """
        response = client.post('/stop_crawling_example_spider')
        assert response.status_code == 200
        assert response.json == {u'scraper_status': u'stopped', u'percentage': 100}

class TestErrorPages(object):
    def test_404_page(self, client):
        """ 404 errors should show the custom 404 page. """
        response = client.get('/nochancethispagewilleverexistintheapp')
        assert_status_with_message(404, response, 'Error 404')
