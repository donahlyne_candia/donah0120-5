# -*- coding: utf-8 -*-

# Scrapy settings for example project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#     http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
#     http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'example'

SPIDER_MODULES = ['example.spiders']
NEWSPIDER_MODULE = 'example.spiders'

WORKING_DIRECTORY_LOGS = '/var/www/web_ui/working_directories/logs'


COOKIES_ENABLED = False
DOWNLOAD_DELAY = 2
CONCURRENT_REQUESTS = 20
CONCURRENT_REQUESTS_PER_DOMAIN = 1

DOWNLOAD_TIMEOUT = 30

# Autothrottle makes the scraper go es fast as possible (by measuring how fast the target site allows us to go)
# ============
# activate it once the scraper is working well for faster testing and faster results

#DOWNLOAD_DELAY = .1 # Autothrottle never goes below this value and so we have to set it to low
#AUTOTHROTTLE_ENABLED = True
#AUTOTHROTTLE_DEBUG = True
#AUTOTHROTTLE_MAX_DELAY = 10.0
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1


DOWNLOADER_MIDDLEWARES = {

    # Other middlewares that might come in handy ..
    # =================
    #'scraping_tools.scrapy.middlewares.blockage_detector.BlockageDetectorMiddleware': 7,
    #'scrapy.downloadermiddlewares.retry.RetryMiddleware': 90,
    #'scrapy.downloadermiddlewares.redirect.RedirectMiddleware': 95,
    #'scraping_tools.scrapy.middlewares.proxy_net.ProxyNetMiddleware': 100,
    #'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': 110,

    # Middlewares to disguise our user agent
    # ======================================x

    'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware' : None,
    #'scraping_tools.scrapy.middlewares.rotate_user_agent.RotateUserAgentMiddleware' :400
}

SPIDER_MIDDLEWARES = {
	'example.log_scrapy_middleware.LogScrapyMiddleware': 1001,
}

# Alternative to rotating the user agent ... disguising as search engine crawler
# ==============================================================================
# to use remove both user agent middleware lines above and uncomment one of these lines

# USER_AGENT = "Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)"
# USER_AGENT = "Googlebot/2.1 (+http://www.googlebot.com/bot.html)"
# USER_AGENT = "Mozilla/5.0 (compatible; bingbot/2.0 +http://www.bing.com/bingbot.htm)"

# Output settings ... we make CSV default
# ===============
FEED_FORMAT = 'csv'
# this can be used to filter and sort the columsn in our output files
#FEED_EXPORT_FIELDS = ["category", "count", "listingUrl", "title", "streetAddress", "postalCode", "locality", "countryName", "companyUrl", "phoneNumber", "faxNumber", "latitude", "longitude", "email"]



# For scraping javascript powered sites - activate this block but also check the pecularities of JsDownloader
# =====================================

#DOWNLOAD_HANDLERS = {
#    'http':  'scraping_tools.scrapy.download_handlers.js_downloader.JsDownloader',
#    'https': 'scraping_tools.scrapy.download_handlers.js_downloader.JsDownloader',
#}

# A webdriver instance is needed for using the JsDownloader
# The easiest option is to install and run phantomjs locally: phantomjs --webdriver='127.0.0.1:8910'
# ... and set the WEBDRIVERS_LIST to this:
#WEBDRIVERS_LIST = ["http://127.0.0.1:8910"]


# VARIOUS ways for overcoming IP blocks
# =====================================
# =====================================

# IMPORTANT: please take these precautions:
# 1. don't use cookies (set COOKIES_ENABLED=False)
# 2. ...

# OPTION 1: dedicated proxies
# =================
# preferred option as we pay per IP and not on usage
# if this doesn't work, we'll have to move on to one of the next options

# as these proxies are dedicated / private, we know for sure nobody else is using these IPs and so if we get blocked,
# it was one of our scraper.

#
# DOWNLOADER_MIDDLEWARES = {
#     'scraping_tools.scrapy.middlewares.blockage_detector.BlockageDetectorMiddleware': 7,
#     'scraping_tools.scrapy.middlewares.proxy_net.ProxyNetMiddleware': 100,
#     'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': 110,
#     'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware' : None,
#     'scraping_tools.scrapy.middlewares.rotate_user_agent.RotateUserAgentMiddleware' :400,
# }
#
# PROXY_USER = '9de8e524f7'
# PROXY_PASSWORD = 'hHxvXVSN'
# # this list might have to be updated from time to time ...
# PROXY_SERVERS = [
#
# '104.144.129.215:4444',
# '104.144.186.131:4444',
# '104.168.2.254:4444',
# '104.227.190.205:4444',
# '107.172.150.115:4444',
# '107.173.129.121:4444',
# '107.183.222.200:4444',
# '23.245.246.107:4444',
# '192.186.174.123:4444',
# '192.227.212.238:4444',
# '45.57.144.196:4444',
# '192.3.168.35:4444',
# '204.86.26.21:4444',
# '23.229.10.237:4444',
# '23.229.22.200:4444',
# '23.244.123.118:4444',
# '23.245.227.247:4444',
# '23.250.1.151:4444',
# '23.254.12.162:4444',
# '23.94.149.175:4444',

# ]
#
# PROXY_LIST_STATIC = [PROXY_USER + ":" + PROXY_PASSWORD + "@" + proxy for proxy in PROXY_SERVERS]


# OPTION 2: proxy-mesh
# ====================
# IMPORTANT: usually only world.proxymesh.com is available. This will use proxies all over the world
# If you need proxies in a particular location (e.g. the United States), let me know and I'll activate this special
# option

# Proxy Mesh automatically rotates many IPs and also refreshes IPs every 24 hours ... so usually we'll have more IPs
# rotating than with our dedicated proxies. Downside: these IPs are shared. So if someone else is scraping the same
# site as we do through proxy mesh, we might get blocked even faster.
# Means Proxy Mesh is perfect for not so well known sites that aren't scraped by other people (examples of sites that
# ARE scraped by many other people and where proxy mesh doesn't work: google, amazon, etc)

#
# DOWNLOADER_MIDDLEWARES = {
#     'scraping_tools.scrapy.middlewares.blockage_detector.BlockageDetectorMiddleware': 7,
#     'scraping_tools.scrapy.middlewares.proxy_net.ProxyNetMiddleware': 100,
#     'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': 110,
#     'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware' : None,
#     'scraping_tools.scrapy.middlewares.rotate_user_agent.RotateUserAgentMiddleware' :400,
# }
#
# PROXY_USER = 'wordbean'
# PROXY_PASSWORD = 'rckbl035'
# PROXY_SERVERS = [#'open.proxymesh.com:31280', # avoid, bad quality
#                       'world.proxymesh.com:31280',
#                       # 'us.proxymesh.com:31280', # needs to be activated
#                      ]
#
# PROXY_LIST_STATIC = [PROXY_USER + ":" + PROXY_PASSWORD + "@" + proxy for proxy in PROXY_SERVERS]
# PROXY_DROP_FAILED = False



# OPTION 3: stormproxies - a much cheaper but also less powerfull alternative to luminati (uses the same concept
# of residential proxies.) .... requires running tunnels.sh to activate port forwarding
# =======================================================================================================
#
# DOWNLOADER_MIDDLEWARES = {
#     'scraping_tools.scrapy.middlewares.proxy_net.ProxyNetMiddleware': 100,
#     'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': 110,
#     'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware': None,
#     'scraping_tools.scrapy.middlewares.rotate_user_agent.RotateUserAgentMiddleware': 400,
# }
#
# PROXY_SERVERS = [
#     # "198.204.229.194:19006",
#     # "104.255.67.241:19008",
#     # "142.54.179.98:19004",
#     # "192.187.125.234:19005",
#     # "199.168.141.147:19003",
#     '127.0.0.1:2000',
#     '127.0.0.1:2001',
#     '127.0.0.1:2002',
#     '127.0.0.1:2003',
#     '127.0.0.1:2004',
#     '127.0.0.1:2005',
#     '127.0.0.1:2006',
#     # '127.0.0.1:2007',
#     '127.0.0.1:2008',
#     '127.0.0.1:2009',
#     '127.0.0.1:2010',
#     '127.0.0.1:2011',
#     '127.0.0.1:2012',
#     '127.0.0.1:2013',
#     '127.0.0.1:2014',
# ]
#
# PROXY_LIST_STATIC = PROXY_SERVERS  # [PROXY_USER + ":" + PROXY_PASSWORD + "@" + proxy for proxy in PROXY_SERVERS]
# PROXY_DROP_FAILED = False
# RETRY_TIMES = 10
#



# #### EXPENSIVE OPTIONS BELOW ########

# Below two expensive solutions that work very well, but are expensive
# IMPORTANT: Use only after asking me (Ruediger) ... or if I told you so


# OPTION 4: crawlera network - works well for google and other sites - prefer over luminati as it is less expensive
# =======================================================================================================

# DOWNLOADER_MIDDLEWARES = {
#     'scraping_tools.scrapy.middlewares.blockage_detector.BlockageDetectorMiddleware': 7,
#     'scrapy_crawlera.CrawleraMiddleware': 600
# }
# CRAWLERA_ENABLED = True
# CRAWLERA_USER = 'd280347b258f4dfcbecc2570143034fb'
# CRAWLERA_PASS = ''
#
# DEFAULT_REQUEST_HEADERS = {
#     'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#     'Accept-Language': 'en',
#     'X-Crawlera-UA': 'desktop',
# }
#
# CONCURRENT_REQUESTS = 200
# CONCURRENT_REQUESTS_PER_DOMAIN = 50
# DOWNLOAD_TIMEOUT = 600


# OPTION 5: luminati network - most expensive but has millions of IPs ... so usually it's the best solution for IP blocks
# =============================================================================================================

# DOWNLOADER_MIDDLEWARES = {
#     'scraping_tools.scrapy.middlewares.blockage_detector.BlockageDetectorMiddleware': 7,
#     'scraping_tools.scrapy.middlewares.luminati.LuminatiMiddleware': 100,
#     'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': 110,
#     'scrapy.downloadermiddlewares.useragent.UserAgentMiddleware' : None,
#     'scraping_tools.scrapy.middlewares.rotate_user_agent.RotateUserAgentMiddleware' :400,
# }
#
#
# LUMINATI_USERNAME = "ruedigerchmidt"
# LUMINATI_PASSWORD = "29b2f737b241"
# LUMINATI_ROTATE_IP = True
# LUMINATI_COUNTRY_CODE = "us"
# LUMINATI_FAKE_REFERER = True








# For finetuning the retry strategy if proxies or target server are unreliable
# =================================

# # Retry many times since proxies often fail
# RETRY_TIMES = 50
# # Retry on most error codes since proxies fail for different reasons
# RETRY_HTTP_CODES = [500, 502, 503, 504, 429, 400, 403, 404, 408, 429, 470, 999]
