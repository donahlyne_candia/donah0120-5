#!/bin/bash

source /usr/local/web/shared/venv/bin/activate
nohup scrapyd &> /var/log/scrapyd.log &