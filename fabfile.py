import os
import sys
from importlib import import_module

#import fabtools
from fabric.contrib.project import rsync_project
from fabric.contrib.console import confirm
from fabric.api import *
from fabric.contrib import files
from contextlib import contextmanager as _contextmanager
import requests

sys.path.append(os.path.join(os.path.dirname(__file__), '.'))

# TODO this needs to be changed
BASE_DIR = '/usr/local/web'
WWW_DIR = BASE_DIR + '/current'
SHARED_DIR = BASE_DIR + '/shared'

ITEMS_TO_UPLOAD = ['requirements.txt', 'web', 'scrapers']

# these packages are usually needed, add more if necessary
REQUIRED_PACKAGES = ["language-pack-de", "language-pack-en", 'build-essential',
                     "python", "python-dev", "python-pip", "python-virtualenv",
                     'libssl-dev', 'libffi-dev',
                     'python3', 'python3-pip',
                     "nginx", 'logrotate',
                     ]


def required():
    require('stage', provided_by=(development, beta, production,))


def stage_set(stage_name='beta'):
    env.stage = stage_name
    config = import_module('deploy.stages.' + stage_name)
    # put all settings as one module into the env
    setattr(env, 'config', config)

    # set fabric settings directly into the env
    fab_vars = ['FAB_FORWARD_AGENT', 'FAB_HOSTS', 'FAB_PASSWORD', 'FAB_USER', 'FAB_DOMAIN', 'FAB_PROJECT_NAME']
    for fab_var in fab_vars:

        if fab_var == 'FAB_HOSTS':
            env.host_string = getattr(config, fab_var)[0]

        setattr(env, fab_var.replace('FAB_', '').lower(), getattr(config, fab_var))

    # env.host_string = '178.62.91.111'


def stop_if_not_sure(message="IMPORTANT WARNING:\n\nThis is a deployment to a production or production like "
                             "environment. Clients will see this. Are you sure to proceed?"):
    proceed = confirm(message,
                      default=False)
    if not proceed:
        sys.exit()


@task
def development():
    stage_set('development')


@task
def beta():
    stop_if_not_sure()
    stage_set('beta')


@task
def production():
    stop_if_not_sure()
    stage_set('production')


# these are the two main tasks
# deploy is run for initial deployment

@task
def deploy():
    stop_services()
    install_requirements()
    web_setting()
    create_folder()
    upload_source_code()
    install_virtualenv()
    upload_config()
    install_required_packages()
    install_certificates()
    activate_services()
    restart_services()
    test_services()


# update is run for later updates
@task
def update():
    stop_services()
    upload_source_code()
    upload_config()
    install_required_packages()
    install_certificates()
    activate_services()
    restart_services()
    test_services()


@task
def activate_letsencrypt_certbot():
    put("deploy/deployment_files/nginx_with_letsencrypt.conf", "/etc/nginx/sites-enabled/default")
    restart_services()
    sudo("yes '2' | certbot --nginx -d %s" % env.domain)

    fabtools.cron.add_daily('letsencrypt_monthly_update', 'root', 'certbot renew')


@task
def deactivate_letsencrypt_certbot():
    put("deploy/deployment_files/nginx.conf", "/etc/nginx/sites-enabled/default")
    restart_services()


@_contextmanager
def virtualenv():
    with cd(env.directory):
        with prefix(env.activate):
            yield


# TODO: some of the code below can be reused, but most must be modified to match the current project
# if you need some of the files referenced here, check fastcollab_flight repository

def install_requirements():
    sudo("apt-get -y update")
    sudo("apt-get -y upgrade")

    r = " ".join(REQUIRED_PACKAGES)
    sudo("apt-get -y install " + r)


def install_certificates():
    sudo("yes "" | add-apt-repository ppa:certbot/certbot")
    run('curl -L https://couchdb.apache.org/repo/bintray-pubkey.asc \
        | sudo apt-key add -')


def create_folder():
    sudo("mkdir -p /var/www")
    sudo("mkdir -p %s" % WWW_DIR)


def upload_source_code():
    sudo('rm -rf %s/*' % WWW_DIR)
    upload_folders = {
        WWW_DIR: ITEMS_TO_UPLOAD
    }
    for dest in upload_folders:
        sudo("mkdir -p %s" % dest)
        for src in upload_folders[dest]:
            put(src.strip(), dest, use_sudo=True)
            
    sudo(r'find %s -name *.pyc -exec rm {} \;' % WWW_DIR)
    sudo(r'find %s -name __pycache__ -exec rm {} \;' % WWW_DIR)

    sudo('chmod +x %s/scrapers/*.sh' % WWW_DIR)
    sudo("mkdir -p %s/working_directories" % SHARED_DIR)
    sudo("mkdir -p %s/working_directories/upload" % SHARED_DIR)
    sudo("mkdir -p %s/working_directories/download" % SHARED_DIR)
    sudo("mkdir -p %s/working_directories/logs" % SHARED_DIR)
    sudo("chown www-data %s/working_directories/upload" % SHARED_DIR)
    sudo("chown www-data %s/working_directories/download" % SHARED_DIR)
    sudo("chown www-data %s/working_directories/logs" % SHARED_DIR)
    sudo("chown www-data %s/working_directories" % SHARED_DIR)
    sudo("chgrp www-data %s/working_directories/upload" % SHARED_DIR)
    sudo("chgrp www-data %s/working_directories/download" % SHARED_DIR)
    sudo("chgrp www-data %s/working_directories/logs" % SHARED_DIR)
    sudo("chgrp www-data %s/working_directories" % SHARED_DIR)


def upload_config():
    upload_api_config()
    upload_logrotate()
    # upload_scrapyd_config()


def upload_scrapyd_config():
    put("deploy/deployment_files/scrapyd.conf", "~/.scrapyd.conf")
    put("deploy/deployment_files/scrapyd_runner.sh", WWW_DIR)


def upload_api_config():
    # nginx config
    put("deploy/deployment_files/nginx.conf", "/etc/nginx/sites-enabled/default")
    put("deploy/deployment_files/passwd", "/etc/nginx/passwd")

    # gunicorn config
    put("deploy/deployment_files/web.service", "/etc/systemd/system/")


def web_setting():
    with open('web/web_settings.py', 'w+') as f:
        f.write('project_name = "%s"' % env.project_name)


def upload_logrotate():
    put("deploy/deployment_files/web.logrotate.conf", "/etc/logrotate.d")

def install_required_packages():
    install_re2()
    install_python_requirements()

def install_re2():
    if not files.exists('/usr/local/re2/Makefile'):
        run("cd /usr/local && git clone https://code.googlesource.com/re2")
        run("cd /usr/local/re2 && make && make install")

def install_virtualenv():
    if not files.exists('%s/venv' % SHARED_DIR):
        sudo("dpkg-reconfigure --frontend=noninteractive locales")
        sudo("virtualenv %s/venv" % SHARED_DIR)

def install_python_requirements():
    sudo("dpkg-reconfigure --frontend=noninteractive locales")
    env.directory = '%s/venv' % SHARED_DIR
    env.activate = 'source %s/venv/bin/activate' % SHARED_DIR
    with virtualenv():
        run("pip install cython")
        run("pip install -r %s/requirements.txt" % WWW_DIR)


def configure_service_paths():
    sudo("mkdir -p /var/log/web")
    sudo("chown www-data /var/log/web")

def activate_services():
    configure_service_paths()
    sudo("systemctl start web")
    sudo("systemctl enable web")

    sudo("update-rc.d -f start_crawler.sh remove")
    sudo("ln -sf %s/scrapers/start_crawler.sh /etc/init.d/" % WWW_DIR)
    sudo("update-rc.d start_crawler.sh start 2")


def stop_services():
    sudo("service web stop ; true")
    sudo("killall -r holdalive ; true")
    sudo("killall scrapy ; true")
    sudo("killall scrapy; true")
    sudo("rmdir /tmp/*.lock; true")

@task
def restart_services():
    sudo("systemctl daemon-reload")
    sudo("service web restart")
    sudo("service nginx restart")
    # sudo("%s/scrapers/start_crawler.sh ; sleep 2" % WWW_DIR)

@task
def test_services():
    for host in env.hosts:
        r = requests.get("http://" + host)
        print r.text
        assert 200 == r.status_code 

@task
def activate_scrapers():
    scrapers_dir = WWW_DIR + '/scrapers/*/'
    except_folders = ['middlewares', 'shared']

    output = run('ls -d ' + scrapers_dir)
    ls = output.split()

    sudo("service scrapyd_runner.sh start")
    with virtualenv():
        for folder in ls:
            if not folder in except_folders:
                sudo("cd " + folder + " && scrapyd-deploy")
